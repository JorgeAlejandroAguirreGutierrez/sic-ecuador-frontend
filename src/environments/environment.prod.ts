export const environment = {
  production: true,
  host:'http://sicecuadorj.us-east-2.elasticbeanstalk.com/api',
  prefijo_url_imagenes: 'http://sicecuadorj.us-east-2.elasticbeanstalk.com/storage/'
};
