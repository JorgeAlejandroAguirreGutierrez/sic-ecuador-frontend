export class GrupoCliente {
    id: number;
    codigo:string;
    denominacion:string;
    numero_cuenta:string;
    nombre_cuenta:string;
    cliente_relacionado:boolean;

    constructor() {
        this.id=0;
    }
}
