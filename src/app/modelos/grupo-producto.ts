export class GrupoProducto {
    id:number;
    codigo: string;
    descripcion: string;
    tipo: string;
    nombre_tabla: string;

    constructor() {
        this.id=0;
    }
}
