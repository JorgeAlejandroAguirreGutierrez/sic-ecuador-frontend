export class CategoriaCliente {
    id:number;
    codigo: string;
    categoria: string;
    abreviatura: string;

    constructor() {
        this.id=0;
    }
    
}
