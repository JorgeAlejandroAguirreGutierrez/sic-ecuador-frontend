export class TarjetaCredito {
  id:number;
  //codigo: string;
  franquicia:string;
  banco:string;
  titular: boolean;
  identificacion:string;
  nombre: string;
  diferido: boolean;
  operador:string;
  lote: string;
  valor: number;
}
