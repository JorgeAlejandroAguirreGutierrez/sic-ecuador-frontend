export class Compensacion {
    id:number;
    //codigo: string;
    tipo_comprobante:string;
    comprobante:string;
    fecha_comprobante: Date;
    origen: string;
    motivo:string;
    fecha_vencimiento: Date;
    valor_inicial: number;
//    saldo_anterior: number;
    valor_compensado: number;
//    saldo: number;
//    compensado: boolean;
    //cliente: number;
}
