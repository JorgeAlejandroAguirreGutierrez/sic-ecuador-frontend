export class Genero {
    id: number;
    codigo: string;
    sexo: string;
    abreviatura: string;

    constructor() {
        this.id=0;
    }
}
