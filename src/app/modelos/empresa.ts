export class Empresa {
    id: number;
    codigo: string;
    identificacion:string;
    razon_social:string;
    logo: string;
    
    constructor() {
        this.id=0;
    }
}
