export class Impuesto {
    id:number;
    codigo:string;
    codigo_norma:string;
    porcentaje: number;

    constructor() {
        this.codigo="";
        this.porcentaje=0;
    }
}
