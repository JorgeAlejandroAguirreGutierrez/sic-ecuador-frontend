export class VehiculoTransporte {
    id:number;
    codigo:string;
    modelo:number;
    placa:string;
    marca:string;
    cilindraje:number;
    clase:string;
    color:string;
    fabricacion:number;
    numero:number;
    activo:boolean;

}
