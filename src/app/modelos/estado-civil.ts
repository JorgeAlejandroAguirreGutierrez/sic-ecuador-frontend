export class EstadoCivil {
    id: number;
    codigo: string;
    estado_civil: string;
    abreviatura: string;

    constructor() {
        this.id=0;
    }
}
