export class Transportista {
    id:number;
    codigo:string;
    nombre:string;
    identificacion:string;
    vehiculo_propio:boolean;
}
