export class Precio {
    id: number;
    codigo: string;
    nombre: string;
    descuento: number;
    valor: number;

    constructor() {
    this.id=0;
    this.valor=0;
    }
}